package sec02.exam08;

public class SystemArraycopy {

	public static void main(String[] args) {
		String[] oldStrArray = {"java","array","copy"};
		String[] newstrArray = new String[5];
		
		System.arraycopy(oldStrArray, 0, newstrArray, 0,oldStrArray.length);
		
		for(int i=0; i<newstrArray.length; i++) {
			System.out.print(newstrArray[i]+ ",");
		}
		
	}

}
