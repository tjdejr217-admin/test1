package sec01.exam6;

public class SwitchNoBreakCaseExample {

	public static void main(String[] args) {
		int time = (int) (Math.random() * 4) + 8;
		System.out.println("[현재시각:" + time + "시]");
		
		switch(time) {
		case 8 :
			System.out.println("8시 입니다");
			
		case 9 :
			System.out.println("9시 입니다");
		
		case 10 :
			System.out.println("10시 입니다");
			
		default :
			System.out.println("11시 입니다");
			
		}

	}

}
