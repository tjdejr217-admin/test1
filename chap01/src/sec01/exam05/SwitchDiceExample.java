package sec01.exam05;

public class SwitchDiceExample {

	public static void main(String[] args) {
		int num = (int) (Math.random() * 6) + 1;
		switch(num) {
		case 1 :
			System.out.println("it's no.1");
			break ;
		case 2 :
			System.out.println("it's no.2");
			break ;
		case 3 :
			System.out.println("it's no.3");
			break ;
		case 4 : 
			System.out.println("it's no.4");
			break ;
		case 5 :
			System.out.println("it's no.5");
			break ;
		default :
			System.out.println("it's no.6");
			break;
		}

	}

}
