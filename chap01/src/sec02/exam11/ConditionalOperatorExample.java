package sec02.exam11;

public class ConditionalOperatorExample {

	public static void main(String[] args) {
		int score = 85;
		char grade = (score > 90) ? 'A' : ((score >80) ? 'B' : 'C' );
		System.out.println(score + "점은" + grade + "등급입니다.");
		
		int sc = 90 ;
		char gr = (sc > 95) ? 'A' : ((sc >90) ? 'B' :(sc >80) ? 'C' : 'D');
		System.out.println(sc + "점은" + gr + "입니다.");

	}

}
